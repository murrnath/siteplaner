<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'default' );

/** MySQL database username */
define( 'DB_USER', 'default' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'siteplan_visualizer' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'dFX14gGSW0jMK=(Sc?NMAHj29s/5Yx`Noqw5>_VqN;Cft9@1mN/uZle%2qhS.Tn-' );
define( 'SECURE_AUTH_KEY',  '3HVYjo]1g+UTj*S$opTK_|)v1iE}f:.z>_{NxW5VpixS[,FgP-=tR:ag*-|gYeZ2' );
define( 'LOGGED_IN_KEY',    '%4(o[;/4UgP)5S}oQicbx(7^7:^l_zBf{]EkZ^!>mY)n-*QO%4:q.7B4j(7$@>jN' );
define( 'NONCE_KEY',        'h@eEKBZ@]-H8#v,<in$gG;$jkNn/1E_jc85E%Zt}PQ`D!%WK$(&%%^:x9SDU+vyj' );
define( 'AUTH_SALT',        'y>E9#qV#!q6lK,`@%J(RY%*5Vc{KiK9mzx]RC18dx@>+^QG*5zyl5J4Mx#<yCd1L' );
define( 'SECURE_AUTH_SALT', 'G@{8)zx_#_dFv5pX-.VeWbL[kt`NdgQoO|-#P0*Di%Dc<0${N^yU$cr5h%KA,z@/' );
define( 'LOGGED_IN_SALT',   'Sta+Xoq7E?|0RV:)^B}F<w[%)vnrwuV;4uM[XUsMF7xP4_>hjV^*j%oh$7!wh<|T' );
define( 'NONCE_SALT',       'DnduQlN<pc^o_t.}Ib&l|Gmfd+t*(hrywsvTF[`4,:P:Y4C7r&42UV&O(Lh>U*,o' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
