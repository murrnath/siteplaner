<?php
include __DIR__."/utilities.php";

/*********************************************************************************
************************************ Menu Changes ********************************
*********************************************************************************/

add_action('admin_menu', 'adminMenuInit');
function adminMenuInit() {
    removeCommentSupport();
    removeWPPages();
    addMenuPages();
    addOptionsPages();
}

function removeWPPages() {
    remove_menu_page('edit.php');
    remove_menu_page('edit.php?post_type=page'); //i might bring this back later to make a documentation page for the site
    remove_menu_page('edit-comments.php');
}

function removeCommentSupport() {
    remove_post_type_support('post', 'comments');
    remove_post_type_support('page', 'comments');
}

function addMenuPages() {
    add_menu_page(
        'Scene Configuration',
        'scene conf',
        'read',
        'scene-conf',
        'adminPageTemplate',
        'dashicons-building',
        5
    );
}

function addOptionsPages() {
    if(function_exists('acf_add_options_page'))
    acf_add_options_page(array(
        'page_title' 	=> 'Scene Settings',
        'menu_title'	=> 'Scene Settings',
        'menu_slug' 	=> 'scene-settings',
        'capability'	=> 'edit_posts',
        'supports'      => 'custom-fields',
        'position'      => 6
    ));
}

function adminPageTemplate() {
    if($_GET['page']) {
        //load the actualy page otherwise we're going to do some kind of 404 thing
        loadAdminTemplate($_GET['page']);
    }
    else {
        //404
    }
}

/*********************************************************************************
************************************ User Changes ********************************
*********************************************************************************/
function removeUserRoles() {
    //remove the default wordpress roles that we do not need
    $roles = array_keys(wp_roles()->roles);

    //remove the administrator role
    unset($roles[array_search('administrator', $roles)]);

    foreach($roles as $role) 
        wp_roles()->remove_role($role);
}


/* setup post types */
function customPostTypes() {
    //just make them all in here i guess
    register_post_type('floorplan', array(
        'label'               => 'Floorplan',
        'description'         => 'Specifications For a Floorplan in the building. These will be hooked up to a floorplan in the building',
        'labels'              => array( 'name' => 'Floorplans', 'singular_name' => 'Floorplan'),
        'supports'            => array( 'title', 'custom-fields'),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 3,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
    ));
}

add_action('init', 'adminInit');
function adminInit() {
    customPostTypes();
    removeUserRoles();
}