const path = require('path');

module.exports = {
    entry: [
        './assets/js/scene-model.js'
    ],
    output: {
        filename: 'scene-model.js',
        path: path.resolve(__dirname, 'dist')
    },
    watch: true,
    watchOptions: {
        poll: 250,
        ignored: ['dist/**', 'node_modules/**']
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'css/[name].css'
                        }
                    },
                    {
                        loader: 'extract-loader'
                    },
                    {
                        loader: 'css-loader?-url'
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    {
                        loader: 'sass-loader'
                    }
                ]
            }
        ]
    }
}