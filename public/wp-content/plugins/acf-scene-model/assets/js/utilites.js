function addClass(ele, cls) {
    if(!ele.classList.contains(cls))
        ele.classList.add(cls);
}

function removeClass(ele, cls) {
    if(ele.classList.contains(cls))
        ele.classList.remove(cls);
}

function hasClass(ele, cls) {
    return ele.classList.contains(cls);
}

function style(ele, property, val) {
    ele.style[property] = val;
}

function ele_remove_text_nodes(ele) {
    var children = ele.childNodes;
    for(var i = 0; i < children.length; i++) {
        if(children[i].nodeType === 3) {
            ele.removeChild(children[i]);
        }
    }
}

function changeText(ele, text) {
    ele_remove_text_nodes(ele);
    ele.appendChild(document.createTextNode(text));
}

function getData(ele, data_id) {
    
    return ele.dataset[data_id.toLowerCase()];
}

function setData(ele, data_id, val) {
    ele.dataset[data_id.toLowerCase()] = val;
}

export default {
    addClass,
    removeClass,
    hasClass,
    style,
    changeText,
    setData,
    getData
};