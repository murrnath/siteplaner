import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

class ThreeDScene {
    constructor(container) {
        this.loader = new GLTFLoader();
        this.activeModel = null;
        this.activeModelSize = new THREE.Vector3();

        var containerRect = container.getBoundingClientRect();
        this.container = container;

        var fov = 70;
        var aspect = containerRect.width / containerRect.height;
        var near = 0.1;
        var far = 1000;

        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);

        this.renderer = new THREE.WebGLRenderer();
        this.container.appendChild(this.renderer.domElement);
        this.setRendererSize();

        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.camera.position.set(0, 20, 100);
        this.camera.lookAt(0, 0, 0);

        var ambientLight = new THREE.AmbientLight(0xFFFFFF, 1);
        this.scene.add(ambientLight);

        if(this.container.dataset.modelurl !== "") {
            this.loadModelIntoScene(this.container.dataset.modelurl);
        }
        this.camera.position.z = 5;
    }

    loadModelIntoScene(url) {
        this.loader.load(url, (gltf) => {
            if(this.activeModel !== null) {
                this.scene.remove(this.activeModel);
                this.activeModel = null;
            }

            this.activeModel = gltf.scene;
            this.scene.add(this.activeModel);

            var box = new THREE.Box3().setFromObject(this.activeModel);
            var boxSize = box.getSize();
            this.activeModelSize.set(boxSize.x, boxSize.y, boxSize.z);
            var sizeEvent = new CustomEvent('modelSize', {detail: {
                size: this.activeModelSize
            }});
            this.container.dispatchEvent(sizeEvent);
        }, undefined, (error) => {
            console.error(error);
        });
    }

    setRendererSize() {
        var containerRect = this.container.getBoundingClientRect();
        this.renderer.setSize(containerRect.width, containerRect.height);
    }

    render() {
        this.renderer.render(this.scene, this.camera);
    }
}

export default ThreeDScene;