import ThreeDScene from './threed-stuff/threedScene';
import ModelUploader from './upload/model-uploader';

var uploader;
var scene;

//i want to make this file cleaner.
//much like how the scene happens all in its own module i want the field stuff
//to happen in its own thing as well

function resizeWindow() {
    scene.setRendererSize();
}

function renderLoop() {
    scene.render();
    window.requestAnimationFrame(renderLoop);
}

function updateModelSize(e) {
    console.log(e.detail.size);
}

function start_container() {
    var container = document.querySelector('.scene-container');

    scene = new ThreeDScene(container);
    uploader = new ModelUploader(scene);

    container.addEventListener('modelSize', updateModelSize);
    window.addEventListener('resize', resizeWindow);
    renderLoop();
}

document.addEventListener('DOMContentLoaded', start_container);