import UT from '../utilites';

//lets just do the upload first
//i will just make this work as it does now no need to go crazy or anything

class ModelUploader {

    constructor(scene, failure = 6000) {
        //i need to give this thing a rool to start searching from i think.
        //it might be best to go look how its even being rendered out right now 
        //before i contiune on at all 
        this.failure = failure;
        this.scene = scene;

        this.upload_input = document.getElementById('scene-upload-input');
        this.upload_button = document.getElementById('scene-upload-button');
        this.upload_error = document.getElementById('scene-upload-error');

        this.select_input = document.getElementById('load-select');
        this.select_button = document.getElementById('scene-select-button');

        this.field_input = document.getElementById('value-id');
        this.field_label = document.getElementById('value-label');

        //just assume that they're already here because we are going to do everything in
        //dom content loaded anyway
        
        this.upload_button.addEventListener('click', () => {this.uploadModel()});
        this.select_button.addEventListener('click', () => {this.selectModel()});
    }

    uploadModel() {
        console.log('upload model');
        console.log(this.upload_input);

        if(this.upload_input.files.length > 0) {
        
        var file = this.upload_input.files[0];
        console.log(file);
        
        if(file.type === 'application/zip') {
            //now we can send our data to the backend

            var form = document.forms[1];
            var oldType = form.enctype;

            form.enctype = 'multipart/form-data';
            var formData = new FormData(form);

            var xhr = new XMLHttpRequest();
            xhr.open("POST", 'http://localhost/model_submit/');

            xhr.onreadystatechange = () => {
            if(xhr.readyState === 4) {                
                var response = JSON.parse(xhr.response);
                form.enctype = oldType;
                
                if(response.success === true) {
                    //change teh scene to the new model
                    UT.changeText(this.field_label, response.foldername);
                    //load a model into the scene here as well
                    //im going to take a reference to the scene into this object
                    this.scene.loadModelIntoScene(response.value);
                    this.field_input.value = response.value;
                    this.addOption(response.foldername, response.value);
                }
                else {
                    //display an error message
                    this.upload_input.value = null;

                    //show the response msg
                    UT.changeText(this.upload_error, response.msg);
                    UT.addClass(this.upload_error, 'show');

                    setTimeout(() => {
                        UT.removeClass(this.upload_error, 'show');
                    }, this.upload_failure);
                }
            }}

            xhr.send(formData);
        }}
    }

    selectModel() {
        var val = this.select_input.options[this.select_input.selectedIndex].value;
        var label =  this.select_input.options[this.select_input.selectedIndex].innerHTML;

        if(val !== "") {
            UT.changeText(this.field_label, label);
            this.field_input.value = val;
        }
    }

    addOption(label, value) {
        var opt = document.createElement('option');
        opt.value = value;
        opt.innerHTML = label;

        this.select_input.add(opt);
    }
}

export default ModelUploader;