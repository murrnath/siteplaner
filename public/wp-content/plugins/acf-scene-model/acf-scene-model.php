<?php

/*
Plugin Name: Advanced Custom Fields: Scene Model
Plugin URI: PLUGIN_URL
Description: SHORT_DESCRIPTION
Version: 1.0.0
Author: Nate Murray
Author URI: https://nates.world
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// exit if accessed directly
if( ! defined( 'ABSPATH' ) ) exit;


// check if class already exists
if( !class_exists('nw_acf_plugin_scene_model') ) :

class nw_acf_plugin_scene_model {
	
	// vars
	var $settings;
	
	
	/*
	*  __construct
	*
	*  This function will setup the class functionality
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	void
	*  @return	void
	*/
	
	function __construct() {
		
		// settings
		// - these will be passed into the field class.
		$this->settings = array(
			'version'	=> '1.0.0',
			'url'		=> plugin_dir_url( __FILE__ ),
			'path'		=> plugin_dir_path( __FILE__ )
		);
		
		
		// include field
		add_action('acf/include_field_types', 	array($this, 'include_field')); // v5
		add_action('acf/register_fields', 		array($this, 'include_field')); // v4
	}
	
	
	/*
	*  include_field
	*
	*  This function will include the field type class
	*
	*  @type	function
	*  @date	17/02/2016
	*  @since	1.0.0
	*
	*  @param	$version (int) major ACF version. Defaults to false
	*  @return	void
	*/
	
	function include_field( $version = false ) {
		
		// support empty $version
		if( !$version ) $version = 4;
		
		
		// load textdomain
		load_plugin_textdomain( 'acf-scene-model', false, plugin_basename( dirname( __FILE__ ) ) . '/lang' ); 
		
		
		// include
		include_once('fields/class-nw-acf-field-scene-model-v' . $version . '.php');
	}
	
}

//lets jus throw in a quick action for catching the submit of the model.
//wp is just awesome like it is and it has a hook for this

function rrmdir($dir) {
    if(is_dir($dir)) {
        $objects = scandir($dir);

        foreach($objects as $object) {
            if($object != '.' && $object != '..') {
                if(is_dir($dir.DIRECTORY_SEPARATOR.$object) && !is_link($dir."/"/$object))
                    rrmdir($dir.DIRECTORY_SEPARATOR.$object);
                else
                    unlink($dir.DIRECTORY_SEPARATOR.$object);
            }
        }
        rmdir($dir);
    }
}

add_action('parse_request', 'model_submit_handler');
function model_submit_handler() {
	if($_SERVER['REQUEST_URI'] == '/model_submit/') {
		if($_FILES['scene_model']) {
			$zip = $_FILES['scene_model'];
			$target_dir = "wp-content/models/";
			$target_file = $target_dir.basename($zip['name']);
			$fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
			$success = false;
			$dir = new stdClass();
			$gltf = new stdClass();
			$names = new stdClass();
			$msg = "";
		
			//check that the model folder exists 
			if(!file_exists('wp-content/models/')) {
				$dir = mkdir('wp_content/models', 0777, true);
			}
		
			if($fileType === 'zip') {
				//we have the right file 
		
				$folderName = $zip['name'];
		
				//strip the extensions from it
				$folderName = str_replace(".zip", "", $folderName);
				$folderName = str_replace("/", "-", $folderName);
				$folderName = str_replace(">", "-", $folderName);
				$folderName = str_replace("<", "-", $folderName);
				$folderName = str_replace("|", "-", $folderName);
				$folderName = str_replace(":", "-", $folderName);
				$folderName = str_replace("&", "-", $folderName);
				$folderName = str_replace(" ", "-", $folderName);
		
				$i = 1;
				$needI = false;
		
				if(file_exists("wp-content/models/$folderName")) {
					$needI = true;
		
					while(file_exists("wp-content/models/$folderName-$i")) {
						$i++;
					}
				}
		
				//we have the folder name lets make the folder
				$folder = "wp-content/models/$folderName";
				if($needI === true) {
					$folder .= "-$i";
					$folderName .= "-$i";
				}
		
				$madeDir = mkdir($folder, 0777, true);
		
				if($madeDir) {
					$dir = $folderName;
		
					$unzip = new ZipArchive;
					$unzip->open($zip['tmp_name']);
					$unzip->extractTo($folder);
					$unzip->close();
		
					$names = scandir($folder);
		
					if(is_array($names)) {
						foreach($names as $fl) {
							if(strpos($fl, 'gltf')) {
								$success = true;
								$file = "$folder/$fl";
								$gltf = "/$folder/$fl";
							}
						}
					}
				}
				else $success = false;
			}
		
			if($success === true) {
		
				if($needI === true) {
					$old = $file;
					$file = str_replace(".gltf", "-$i.gltf", $file);
					$gltf = str_replace(".gltf", "-$i.gltf", $gltf);
		
					$changed = rename($old, $file);
		
					if(!$changed) {
						$success = false;
						$msg = "could not rename the gltf file, most likely file permissions";
					}
				}
			}
			
			if($success === false) {
				//remove the directory
				rrmdir($folder);
				if($msg !== "") 
				$msg = "You must have the .gltf in the root of the zip. DO not place it inside of another directory";
			}
		
			echo json_encode(array(
				'success' => $success,
				'foldername' => $dir,
				'value' => $gltf,
				'hot_link' => get_site_url().$gltf,
				'msg' => $msg
			));
			die();
		}
	}
}

// initialize
new nw_acf_plugin_scene_model();


// class_exists check
endif;
	
?>